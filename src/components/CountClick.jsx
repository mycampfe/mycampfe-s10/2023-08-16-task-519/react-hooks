import { useEffect, useState } from "react";

var effect = 1;

const CountClick = () => {
    const [count, setCount] = useState(0);

 
    const btnClickedHandler = () => {
        console.log(count%2)
        if(count%2 === 0) {
            effect ++;
        }

        setCount(count + 1);
    }

    useEffect(() => {
        console.log("useEffect 1");

        document.title = `You clicked ${count} times`;
    })

    useEffect(() => {
        console.log("useEffect 2");
    }, []);

    useEffect(() => {
        console.log("useEffect 3");
    }, [effect])

    return (
        <div>
            <p>You clicked {count} times</p>
            <br />
            <button onClick={btnClickedHandler}>Click here!</button>
        </div>
    )
}

export default CountClick;