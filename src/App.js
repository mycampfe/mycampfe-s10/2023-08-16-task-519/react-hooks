import CountClick from "./components/CountClick";
import InputForm from "./components/InputForm";

function App() {
  return (
    <div style={{margin: "100px"}}>
      <CountClick />

      <InputForm />
    </div>
  );
}

export default App;
